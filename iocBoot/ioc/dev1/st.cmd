< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("DEVICE_IP",                    "bd-fg04.cslab.esss.lu.se")
epicsEnvSet("DEVICE_IPPORT",                "1394")
epicsEnvSet("LOCATION",                     "LAB")
epicsEnvSet("DEVICE_NAME",                  "WFG-004")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME)")
epicsEnvSet("PORT",                         "$(PREFIX)-asyn-port")
epicsEnvSet("STREAM_PROTOCOL_PATH",         "$(DB_DIR)")

# ?? epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")

#- Create a asyn driver
drvAsynIPPortConfigure("$(PORT)","$(DEVICE_IP):$(DEVICE_IPPORT)", 0, 0, 0)

# Load needed records
dbLoadRecords("keithley6221.db", "P=$(PREFIX),R=,PORT=$(PORT)")

# report streamdevice errors
var streamError 1
# report streamdevice debug messages (LOTS!)
# var streamDebug 1

###############################################################################
iocInit
###############################################################################
date
###############################################################################
